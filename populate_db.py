import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'finplan.settings')

import django
django.setup()

from django.utils import timezone
from expensetracker.models import Category, Expense

def populate():
    restaurant_category = add_category('Restaurant')
    food_category = add_category('Food')
    parking_category = add_category('Parking')
    travel_category = add_category('Travel')
    personal_category = add_category('Personal')
    transportation_category = add_category('Transportation')

    add_expense(summary="Yabu",
                amount=1254.56,
                transaction_date=timezone.now(),
                category=restaurant_category)

    add_expense(summary="Saboten",
                amount=2546.56,
                transaction_date=timezone.now(),
                category=restaurant_category)

    add_expense(summary="Spiral",
                amount=8562.56,
                transaction_date=timezone.now(),
                category=restaurant_category)

    add_expense(summary="Fishballs",
                amount=10.56,
                transaction_date=timezone.now(),
                category=food_category)

    add_expense(summary="Mangga at Bagoong",
                amount=1254.56,
                transaction_date=timezone.now(),
                category=food_category)

    add_expense(summary="Greenbelt parking",
                amount=50.00,
                transaction_date=timezone.now(),
                category=parking_category)

    add_expense(summary="Hongkong Trip",
                amount=24587.56,
                transaction_date=timezone.now(),
                category=travel_category)

    add_expense(summary="Ace hardware stuff",
                amount=555.56,
                transaction_date=timezone.now(),
                category=personal_category)

    add_expense(summary="Grab Car",
                amount=541.56,
                transaction_date=timezone.now(),
                category=transportation_category)

    # Print out what we have added to the user.
    for c in Category.objects.all():
        for e in Expense.objects.filter(category=c):
            print ("- {0} - {1}".format(str(c), str(e)))

def add_expense(summary, amount, transaction_date, category):
    e = Expense.objects.get_or_create(summary = summary, transaction_date=transaction_date, category=category)[0]
    e.amount = amount
    e.save()
    return e

def add_category(name):
    c = Category.objects.get_or_create(name=name)[0]
    return c

# Start execution here!
if __name__ == '__main__':
    print
    "Starting FinPlan population script..."
    populate()