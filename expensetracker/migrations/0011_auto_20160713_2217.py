# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-07-13 14:17
from __future__ import unicode_literals

from django.db import migrations, models
import expensetracker.models


class Migration(migrations.Migration):

    dependencies = [
        ('expensetracker', '0010_auto_20160712_1147'),
    ]

    operations = [
        migrations.AlterField(
            model_name='expense',
            name='amount',
            field=models.DecimalField(decimal_places=2, default=0, max_digits=15, validators=[expensetracker.models.validate_positive]),
        ),
        migrations.RemoveField(
            model_name='expense',
            name='category',
        ),
        migrations.AddField(
            model_name='expense',
            name='category',
            field=models.ManyToManyField(to='expensetracker.Category'),
        ),
    ]
