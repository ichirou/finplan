from .models import UserProfile, Category, Expense
from django.contrib.auth.models import User
from django import forms
from django.utils import timezone

class UserForm(forms.ModelForm):
    username = forms.CharField(help_text="Please enter a username.")
    email = forms.CharField(help_text="Please enter your email.")
    password = forms.CharField(widget=forms.PasswordInput(), help_text="Please enter a password.")

    class Meta:
        model = User
        fields = ['username', 'email', 'password']

class UserProfileForm(forms.ModelForm):
    website = forms.URLField(help_text="Please enter your website.", required=False)

    class Meta:
        model = UserProfile
        fields = ['website']

class ExpenseForm(forms.ModelForm):
    summary = forms.CharField(help_text="Please enter transaction summary.")
    amount = forms.DecimalField(decimal_places=2, max_digits=15, help_text="Please enter transaction amount.")
    transaction_date = forms.DateField(widget=forms.SelectDateWidget, help_text="Please enter transaction date", initial=timezone.now())
    category = forms.ModelChoiceField(Category.objects.all(), help_text="Please select a category")

    class Meta:
        model = Expense
        fields = ['summary', 'amount', 'transaction_date', 'category']