from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.views.generic.base import RedirectView
from . import views

app_name = 'expensetracker'
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login/$', views.user_login, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^expenses/$', login_required(views.ExpensesView.as_view()), name='expenses'),
    url(r'^add_expense/$', views.add_expense, name='add_expense'),
    #url(r'^.*', RedirectView.as_view(url='/expensetracker/', permanent=False), name='index'),
    #url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),
    #url(r'^(?P<pk>[0-9]+)/results/$', views.ResultsView.as_view(), name='results'),
    #url(r'^(?P<question_id>[0-9]+)/vote/$', views.vote, name='vote'),
]
