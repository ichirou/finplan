from django.test import TestCase
from django.test import Client
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

class IndexViewTests(TestCase):

    def test_index_view_not_logged_in(self):
        response = self.client.get(reverse('expensetracker:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Hello! Please login..")

    def test_index_view_logged_in(self):
        User.objects.create_superuser(username='admin', email='admin@example.com', password='pass1234')
        c = Client()
        c.login(username='admin', password='pass1234')
        response = c.get(reverse('expensetracker:index'))
        #print (response.context)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "Hello admin")
