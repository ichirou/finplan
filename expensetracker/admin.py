from django.contrib import admin
from .models import UserProfile, Expense, Category, Account, Transaction, Tag

class ExpenseAdmin(admin.ModelAdmin):
    list_display = ('summary', 'amount', 'transaction_date')
    filter_horizontal = ('category',)

class TransactionAdmin(admin.ModelAdmin):
    list_display = ('description', 'transaction_type', 'amount', 'get_tags', 'date', 'account', 'user')
    filter_horizontal = ('tags',)

class AccountAdmin(admin.ModelAdmin):
    list_display = ('name', 'account_type', 'currency', 'balance', 'user')

admin.site.register(UserProfile)
admin.site.register(Expense, ExpenseAdmin)
admin.site.register(Category)
admin.site.register(Account, AccountAdmin)
admin.site.register(Transaction, TransactionAdmin)
admin.site.register(Tag)