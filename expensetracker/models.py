# coding=utf-8
from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.core.validators import DecimalValidator

def validate_positive(value):
    if value < 0:
        raise ValidationError(
            _('%(value)s is not a positive number'),
            params={'value': value},
        )

class UserProfile(models.Model):
    # This line is required. Links UserProfile to a User model instance.
    user = models.OneToOneField(User)

    # The additional attributes we wish to include.
    website = models.URLField(blank=True)
    #picture = models.ImageField(upload_to='profile_images', blank=True)

    # Override the __unicode__() method to return out something meaningful!
    def __str__(self):
        return self.user.username

class Account(models.Model):
    ACCOUNT_TYPES = (
        ('Checking', 'Checking'),
        ('Savings', 'Savings'),
        ('Credit Card', 'Credit Card'),
        ('Cash', 'Cash'),
        ('Investment', 'Investment'),
        ('Loan', 'Loan'),
        ('CD', 'CD'),
        ('Real Estate', 'Real Estate'),
        ('Other', 'Other')
    )
    CURRENCIES = (
        ('ALL', 'Albania Lek, ALL'),
        ('AFN','Afghanistan Afghani, AFN'),
        ('ARS', 'Argentina Peso, ARS'),
        ('AWG', 'Aruba Guilder, AWG'),
        ('AUD', 'Australia Dollar, AUD'),
        ('AZN', 'Azerbaijan New Manat, AZN'),
        ('BSD', 'Bahamas Dollar, BSD'),
        ('BBD', 'Barbados Dollar, BBD'),
        ('BYR', 'Belarus Ruble, BYR'),
        ('BZD', 'Belize Dollar, BZD'),
        ('BMD', 'Bermuda Dollar, BMD'),
        ('BOB', 'Bolivia Bolíviano, BOB'),
        ('BAM', 'Bosnia and Herzegovina Convertible, BAM'),
        ('BWP', 'Botswana Pula, BWP'),
        ('BGN', 'Bulgaria Lev, BGN'),
        ('BRL', 'Brazil Real, BRL'),
        ('BND', 'Brunei Darussalam Dollar, BND'),
        ('KHR', 'Cambodia Riel, KHR'),
        ('CAD', 'Canada Dollar, CAD'),
        ('KYD', 'Cayman Islands Dollar, KYD'),
        ('CLP', 'Chile Peso, CLP'),
        ('CNY', 'China Yuan Renminbi, CNY'),
        ('COP', 'Colombia Peso, COP'),
        ('CRC', 'Costa Rica Colon, CRC'),
        ('HRK', 'Croatia Kuna, HRK'),
        ('CUP', 'Cuba Peso, CUP'),
        ('CZK', 'Czech Republic Koruna, CZK'),
        ('DKK', 'Denmark Krone, DKK'),
        ('DOP', 'Dominican Republic Peso, DOP'),
        ('XCD', 'East Caribbean Dollar, XCD'),
        ('EGP', 'Egypt Pound, EGP'),
        ('SVC', 'El Salvador Colon, SVC'),
        ('EUR', 'Euro Member Countries, EUR'),
        ('FKP', 'FalklandIslands (Malvinas) Pound, FKP'),
        ('FJD', 'Fiji Dollar, FJD'),
        ('GHS', 'Ghana Cedi, GHS'),
        ('GIP', 'Gibraltar Pound, GIP'),
        ('GTQ', 'Guatemala Quetzal, GTQ'),
        ('GGP', 'Guernsey Pound, GGP'),
        ('GYD', 'Guyana Dollar, GYD'),
        ('HNL', 'Honduras Lempira, HNL'),
        ('HKD', 'Hong Kong Dollar, HKD'),
        ('HUF', 'Hungary Forint, HUF'),
        ('ISK', 'Iceland Krona, ISK'),
        ('INR', 'India Rupee, INR'),
        ('IDR', 'Indonesia Rupiah, IDR'),
        ('IRR', 'Iran Rial, IRR'),
        ('IMP', 'Isle of Man Pound, IMP'),
        ('ILS', 'Israel Shekel, ILS'),
        ('JMD', 'Jamaica Dollar, JMD'),
        ('JPY', 'Japan Yen, JPY'),
        ('JEP', 'Jersey Pound, JEP'),
        ('KZT', 'Kazakhstan Tenge, KZT'),
        ('KPW', 'Korea (North) Won, KPW'),
        ('KRW', 'Korea (South) Won, KRW'),
        ('KGS', 'Kyrgyzstan Som, KGS'),
        ('LAK', 'Laos Kip, LAK'),
        ('LBP', 'Lebanon Pound, LBP'),
        ('LRD', 'Liberia Dollar, LRD'),
        ('MKD', 'Macedonia Denar, MKD'),
        ('MYR', 'Malaysia Ringgit, MYR'),
        ('MUR', 'Mauritius Rupee, MUR'),
        ('MXN', 'Mexico Peso, MXN'),
        ('MNT', 'Mongolia Tughrik, MNT'),
        ('MZN', 'Mozambique Metical, MZN'),
        ('NAD', 'Namibia Dollar, NAD'),
        ('NPR', 'Nepal Rupee, NPR'),
        ('ANG', 'Netherlands Antilles Guilder, ANG'),
        ('NZD', 'New Zealand Dollar, NZD'),
        ('NIO', 'Nicaragua Cordoba, NIO'),
        ('NGN', 'Nigeria Naira, NGN'),
        ('NOK', 'Norway Krone, NOK'),
        ('OMR', 'Oman Rial, OMR'),
        ('PKR', 'Pakistan Rupee, PKR'),
        ('PAB', 'Panama Balboa, PAB'),
        ('PYG', 'Paraguay Guarani, PYG'),
        ('PEN', 'Peru Sol, PEN'),
        ('PHP', 'Philippines Peso, PHP'),
        ('PLN', 'Poland Zloty, PLN'),
        ('QAR', 'Qatar Riyal, QAR'),
        ('RON', 'Romania New Leu, RON'),
        ('RUB', 'Russia Ruble, RUB'),
        ('SHP', 'Saint Helena Pound, SHP'),
        ('SAR', 'Saudi Arabia Riyal, SAR'),
        ('RSD', 'Serbia Dinar, RSD'),
        ('SCR', 'Seychelles Rupee, SCR'),
        ('SGD', 'Singapore Dollar, SGD'),
        ('SBD', 'Solomon Islands Dollar, SBD'),
        ('SOS', 'Somalia Shilling, SOS'),
        ('ZAR', 'South Africa Rand, ZAR'),
        ('LKR', 'Sri Lanka Rupee, LKR'),
        ('SEK', 'Sweden Krona, SEK'),
        ('CHF', 'Switzerland Franc, CHF'),
        ('SRD', 'Suriname Dollar, SRD'),
        ('SYP', 'Syria Pound, SYP'),
        ('TWD', 'Taiwan New Dollar, TWD'),
        ('THB', 'Thailand Baht, THB'),
        ('TTD', 'Trinidad and Tobago Dollar, TTD'),
        ('TRY', 'Turkey Lira, TRY'),
        ('TVD', 'Tuvalu Dollar, TVD'),
        ('UAH', 'Ukraine Hryvnia, UAH'),
        ('GBP', 'United Kingdom Pound, GBP'),
        ('USD', 'United States Dollar, USD'),
        ('UYU', 'Uruguay Peso, UYU'),
        ('UZS', 'Uzbekistan Som, UZS'),
        ('VEF', 'Venezuela Bolivar, VEF'),
        ('VND', 'Vietnam Dong, VND'),
        ('YER', 'Yemen Rial, YER'),
        ('ZWD', 'Zimbabwe Dollar, ZWD'),
    )
    name = models.CharField(max_length=128, unique=True)
    account_type = models.CharField(max_length=64, choices=ACCOUNT_TYPES)
    currency = models.CharField(max_length=3, choices=CURRENCIES)
    balance = models.DecimalField(max_digits=15, decimal_places=2)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    def get_transactions(self):
        return self.transactions_set.filter(user=self.user)

class Tag(models.Model):
    name = models.CharField(max_length=128, unique=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

class Transaction(models.Model):
    TRANSACTION_TYPES = (
        ('EXP', 'Expense'),
        ('INC', 'Income'),
        ('TRN', 'Transfer'),
    )
    transaction_type = models.CharField(max_length=3, choices=TRANSACTION_TYPES)
    description = models.CharField(max_length=128)
    amount = models.DecimalField(max_digits=15, decimal_places=2)
    tags = models.ManyToManyField(Tag)
    date = models.DateTimeField()
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE, default=1)

    def get_tags(self):
        return ", ".join([t.name for t in self.tags.all()])

    get_tags.short_description = "Tags"

    def __str__(self):  # __unicode__ on Python 2
        return self.description

class Category(models.Model):
    name = models.CharField(max_length=128, unique=True)

    def __str__(self):  # __unicode__ on Python 2
        return self.name

    class Meta:
        verbose_name_plural = "categories"

class Expense(models.Model):
    summary = models.CharField(max_length=128)
    amount = models.DecimalField(decimal_places=2, max_digits=15, default=0, validators=[validate_positive])
    transaction_date = models.DateField()
    category = models.ManyToManyField(Category)

    def __str__(self):  # __unicode__ on Python 2
        return self.summary
