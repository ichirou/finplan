"""finplan URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include, url, patterns
from django.http import HttpResponseRedirect
from django.contrib import admin
from rest_framework import routers
from expensetracker import views
from django.conf import settings

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'groups', views.GroupViewSet)
router.register(r'expenses', views.ExpensesViewSet)
router.register(r'categories', views.CategoriesViewSet)

urlpatterns = [
    url(r'^$', lambda r: HttpResponseRedirect('expensetracker/')),
    url(r'^expensetracker/api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^polls/', include('polls.urls')),
    url(r'^expensetracker/', include('expensetracker.urls')),
    url(r'^admin/', admin.site.urls),
]

# if settings.DEBUG:
#     urlpatterns += patterns(
#         'django.views.static',
#         (r'media/(?P<path>.*)',
#         'serve',
#         {'document_root': settings.MEDIA_ROOT}), )