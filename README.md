# README #

### What is this repository for? ###

Demo application

AWS deployed demo application: [http://finplan-env.cbqp32vphj.us-west-2.elasticbeanstalk.com/]

Contact me for admin login details for API access.

### How do I get set up? ###

Summary of local set up (Windows)

1. Install Git

    * https://git-scm.com/downloads
    * Follow on-screen instructions for installation

1. Clone the repository

    * Open a command-line window and create the project directory

        `mkdir finplan`

        `cd finplan`

    * Clone the repository using the command below

        `git clone https://bitbucket.org/ichirou/finplan.git`

1. Install Python

    * https://www.python.org/downloads/ (Python 3.5 recommended)
    * Follow on-screen instructions
    * Make sure Python scripts and directory is set in your PATH environment variable (directory name will be different for different version of python)

        `...\Python\Python35-32\Scripts\`

        `...\Python\Python35-32\`

    * Enter `python --version` in command line to check if python is installed correctly.

1. Install virtualenv package and virtualenv wrapper

    * `pip install virtualenv`
    * `pip install virtualenvwrapper-win`

1. Setup and activate virtual environment

    * `mkvirtualenv finplan`
    * Command-line will have a name (finplan) to indicate successful virtual environment activation

1. Install Django and Django rest framework

    * `pip install Django`

    * `pip install djangorestframework`

    * or just use `pip install -r requirements.txt`

1. Update migration of database

    * `cd finplan`

    * `python manage.py makemigrations`

    * `python manage.py migrate`

1. You can create your own admin account for administration login and API access

    * `python manage.py createsuperuser`

1. Run the server

    * `python manage.py runserver`

    * Open http://127.0.0.1:8000/ to access the web application

    * Open http://127.0.0.1:8000/polls to access the polls application
 
###How to run tests###

    `python manage.py test`

###Best practices###

* Automated tests are created to verify functionality and that future changes does not break working features.
* A virtual environment (virtualenv) is used to isolate Python and to keep the dependencies required by different projects in separate place.
* Usage of requirements.txt using pip freeze in virtualenv for AWS to replicate the dependencies.
* A version control system (GIT) is used to track changes and version history.
* User authentication is used in accessing the APIs.
* Usage of admin site to add, change and delete content for site administrators.
* Usage of shortcuts like render() and get_object_or_404() for commonly used operations.
* Raising a 404 error when the data doesn't exist
* Using {% url %} template tag to remove hard coded urls in templates
* Namespacing Urls to differentiate url names in between different apps
* Usage of generic views: ListView and DetailView, less code is better
* Usage of {% csrf_token %} for cross site forgery protection

###Frameworks used###

Django REST framework

http://www.django-rest-framework.org/

### Who do I talk to? ###
* Richmond Ko (richmond.ko@outlook.ph)